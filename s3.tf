resource "aws_s3_bucket" "megaloin1" {
  bucket = "megaloinnp1"
  acl    = "private"

  tags = {
    Name        = "Megaloin_1"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket" "megaloin2" {
  bucket = "megaloinnp2"
  acl    = "private"

  tags = {
    Name        = "Megaloin_2"
    Environment = "Dev"
  }
}

output "resn" {
  value = aws_s3_bucket.megaloin1.arn
}